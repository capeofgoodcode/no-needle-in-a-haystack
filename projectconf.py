'''Declares the configuration for a Detangle project.'''
# imports omitted...

ALLOWED_PROJECT_NAME_RE = re.compile(r'[^\\/?%*:|"<>]+')

def DeclareConfig() -> Config:
    '''Declares project configuration.'''
    fileNameIncludeGroup = 'filenameIncludeGroup'
    fileNameExcludeGroup = 'filenameExcludeGroup'
    dirIncludeGroup = 'dirIncludeGroup'
    dirExcludeGroup = 'dirExcludeGroup'
    advancedGroup = 'advanced'
    issueTrackers = [issueTracker.type for issueTracker
                     in ExtensionPoint(IssueTracker)]
    repositories = ['git'] + [repository.name for repository
                              in ExtensionPoint(RepoAdapter)]
    def validateMetricsGranularities(granularities: List[str]) -> None:
        for granularity in granularities:
            DateGranularity.fromString(granularity)
    config = Config()
    (config
     # ========================================================================
     # project
     # ========================================================================
     .section(
         'project', title='Project')
     .option(
         'name', str, pattern=ALLOWED_PROJECT_NAME_RE.pattern,
         title='Project Name')
     # ========================================================================
     # repository
     # ========================================================================
     .section(
         'repository', title='Repository')
     .option(
         'type', SectionRef, enumeration=repositories,
         title='Type',
         description=('Type of repository to analyze. '
                          'Following repositories are supported: {}. '
                          'For the specified repository a corresponding '
                          'section is expected.'
                          .format(','.join(repositories))))
     .option(
         'url', str,
             title='URL',
             description='Base URL of the repository to analyze.')
     .option('username', str, isOptional=True, default=None, title='Username')
     .option('password', str, isOptional=True, default=None, title='Password')
     # ========================================================================
     # git
     # ========================================================================
     .section(
         'git',
         title='Git')
     .option(
         'revisionRange', str, isOptional=True, default=None,
         title='Revision Range',
         description=('Any value that can be passed as <revision range> '
                          'argument to git log. Example: master -100'))
     .option('since', datetime, isOptional=True, default=None,
             title='Since',
             description=('Capture revisions with a date equal or more recent '
                          'than the specified one. See --since argument of '
                          'git log. Example: Tue Nov 21 00:08:53 2017 +0100 '
                          'or 2017-11-21T00:08:53+01:00'))
     .option('until', datetime, isOptional=True, default=None,
             title='Until',
             description=('Capture revisions with a date equal or older than '
                          'the specified one. See --until argument of git log.'
                          ' Example: Tue Nov 21 00:08:53 2017 +0100 or '
                          '2017-11-21T00:08:53+01:00'))
     # ========================================================================
     # repository_filter
     # ========================================================================
     .section('repository_filter',
              title='Repository Filter')
     .option('fileNameIncludeGlobs', [str], isOptional=True,
             mutuallyExclusiveGroup=fileNameIncludeGroup,
             title='File name include globs',
             description=('Include only file names which match the given '
                          'glob expressions. IMPORTANT: this filter is applied'
                          ' only to file names. Must not be specified together'
                          ' with fileNameIncludeRegex. Example: *.cpp *.c'))
     .option('fileNameIncludeRegex', Regex, isOptional=True,
             mutuallyExclusiveGroup=fileNameIncludeGroup,
             title='File name include regular expression',
             description=('Include only file names which match the given '
                          'regular expression. IMPORTANT: this filter is '
                          'applied only to file names. Must not be specified '
                          'together with fileNameIncludeGlobs. Example: '
                          r'(.*\.(cpp|c|h|)$'))
     .option(
         'fileNameExcludeGlobs', [str], isOptional=True,
             mutuallyExclusiveGroup=fileNameExcludeGroup,
             title='File name exclude globs',
             description=('Do not consider file names which match the given '
                          'glob expressions. IMPORTANT: this filter is applied'
                          ' only to to file names. Must not be specified '
                          'together with fileNameExcludeRegex. Has precedence '
                          'over fileNameInclude* options, i.e. if same file '
                          'name is matched by both, it is still excluded.'
                          'Example: *.txt *.exe'))
     .option(
         'fileNameExcludeRegex', Regex, isOptional=True,
             mutuallyExclusiveGroup=fileNameExcludeGroup,
             title='File name exclude regular expression',
             description=('Do not consider file names which match the given '
                          'regular expression. IMPORTANT: this filter is '
                          'applied only to file names. Must not be specified '
                          'together with fileNameExcludeGlobs. Has precedence '
                          'over fileNameInclude* options, i.e. if same file '
                          'name is matched by both, it is still excluded.'
                          r'Example: (.*\.(txt|exe|))$'))
     .option(
         'dirIncludeGlobs', [str], isOptional=True,
             mutuallyExclusiveGroup=dirIncludeGroup,
             title='Directory include globs',
             description=('Include only directories which match the given '
                          'glob expressions. Must not be specified together '
                          'with dirIncludeRegex. Example: *./src/*'))
     .option(
         'dirIncludeRegex', Regex, isOptional=True,
             mutuallyExclusiveGroup=dirIncludeGroup,
             title='Directory include regular expression',
             description=('Include only directories names which match the '
                          'given regular expression. Must not be specified '
                          'together with dirIncludeGlobs. Example: '
                          '.*/src(/.*)?'))
     .option(
         'dirExcludeGlobs', [str], isOptional=True,
             mutuallyExclusiveGroup=dirExcludeGroup,
             title='Directory exclude globs',
             description=('Do not consider directories which match the given '
                          'glob expressions. Must not be specified together '
                          'with dirExcludeRegex. Has precedence over '
                          'dirInclude* options, i.e. if same directory is '
                          'matched by both, it is still excluded.'
                          'Example: *.doc/*'))
     .option(
         'dirExcludeRegex', Regex, isOptional=True,
             mutuallyExclusiveGroup=dirIncludeGroup,
             title='Directory exclude regular expression',
             description=('Do not consider directories which match the given '
                          'regular expression. Must not be specified together '
                          'with dirExcludeGlobs. Has precedence over'
                          'dirInclude* options, i.e. if same directory is '
                          'matched by both, it is still excluded. '
                          'Example: .*/doc(/.*)?'))
     # ========================================================================
     # issues
     # ========================================================================
     .section(
         'issues',
              title='Issues')
     .option(
         'issueTracker', SectionRef,
             enumeration=issueTrackers,
             isOptional=True,
             title='Issue tracker',
             description=('Type of the issue tracker. '
                          'Following issue trackers are supported: {}. '
                          'If specified a corresponding section is expected.'
                          .format(','.join(issueTrackers))))
     .option(
         'commitIssueIdFilter', SectionRef,
             sectionType='CommitIssueIdFilter',
             isOptional=True, default=['example-issue-filter'],
             isMultiple=True,
             description=('Filters to be used for extracting issue IDs from '
                          'commit messages.'))
     .option(
         'ignoreCase', bool, default=True,
             description=('Specifies whether detected issue IDs should be '
                          'treated in case-sensitive manner or not. If True, '
                          'strings such as "DET-123" and "Det-123" are treated'
                          ' as equal issue IDs. ATTENTION: if True, the '
                          ' pattern settings in commitIssueIdFilter sections '
                          'are used in case-insensitive mode.'))
     .option(
         'followInwardLinks', Regex, isOptional=True,
             description=('Regular expression to match types of inward issue '
                          'links. Matching inward links will be followed '
                          'during issue ID extraction and the target issue ID '
                          'will be added to the set of extracted issue IDs. '
                          'Example: (subtask)'))
     .option(
         'followOutwardLinks', Regex, isOptional=True,
             description=('Regular expression to match types of outward issue '
                          'links. Matching outward links will be followed '
                          'during issue ID extraction and the target issue ID '
                          'will be added to the set of extracted issue IDs. '
                          'Example: (epic)'))
     .option(
         'levelizeIssues', bool, default=False,
             description=('Compute levels for nested issues according to '
                          'followInwardLinks and followOutwardLinks options '
                          'and append the level to the issue type.'))
     .section(
         'example-issue-filter', sectionType='CommitIssueIdFilter',
              isOptional=True,
              description=('Contains filter parameters for extracting issue '
                           'IDs from commit messages.'))
     .option(
         'pattern', Regex, default=re.compile('([0-9]+)'),
             description=('Regular expression used to extract the issue IDs '
                          'from a commit message. Each matched group will be '
                          'included as an issue ID into the result.'))
     .option(
         'ignoreCase', bool, default=True, isHidden=True,
             description='Specifies whether the pattern should be used in '
                         'case-insensitive mode.')
     .option(
         'since', datetime, isOptional=True, default=None,
             description=('Applies the entire filter only if the specified '
                          'date/time is equal or more recent than the date of '
                          'the corresponding commit. If no timezone data is '
                          'contained in the specified string, the local '
                          'timezone is assumed.'))
     .option(
         'until', datetime, isOptional=True, default=None,
             description=('Applies the entire filter only if the specified '
                          'date/time is equal or older than the date of the'
                          'corresponding commit. If no timezone data is '
                          'contained in the specified string, the local '
                         'timezone is assumed.'))
     .option(
         'minLine', int, minValue=1, isOptional=True,
             description=('Integer specifying a line number in the commit '
                          'message. Filter will be applied only to line '
                          'numbers which are equal or greater than the '
                          'specified one. The first line has the number 1'))
     .option(
         'maxLine', int, minValue=1, isOptional=True,
            description=('Integer specifying a line number in the commit '
                         'message. Filter will be applied only to line '
                         'numbers which are equal or lower than the '
                         'specified one. The first line has the number 1'))
     .option(
         'replace', str, isOptional=True, isExtended=True,
             default={'groupName': 'replacementString'},
             description=normalizeStr(
                 '''The content of named groups in the pattern can be
                 replaced by arbitrary (also empty) strings. For
                 this purpose an arbitrary number of '
                 replace.<group> = <str> options my be specified.
                 Examples:
                 commit message: "X-_-2346"
                 pattern=(X(?P<remove>[^0-9]*)[0-9]+)
                 replace.remove =
                 extracted issue ID: "X2346"'

                 commit message: "TaskA12B3"
                 pattern=(Bug(?P<abc>A)23(?P<def>B)9)
                 replace.abc =
                 replace.def = xyz
                 extracted issue ID: "Bug23xyz9"'''))
     # ========================================================================
     # metrics
     # ========================================================================
     .section(
         'metrics', sectionGroup=advancedGroup)
     .option(
         'granularities', [str], title='Date Granularity', isOptional=True,
             default=['year'], validator=validateMetricsGranularities,
             description=('Space separated list of date granularties to '
                          'compute metrics for. Possible values: {}'
                          .format(', '.join(g.name for g in
                                            DateGranularity.granularities()))))
     # ========================================================================
     # git_advanced
     # ========================================================================
    .section(
        'git_advanced', sectionGroup=advancedGroup,
             title='Advanced Git settings')
    .option(
        'renameSimilarityRatio', float, minValue=0, maxValue=1.0,
            isOptional=True, default=0.1,
            title='Rename similarity ratio',
            description= """Value between 0 and 1 specifying the minimal 
                         similarity between two files required to recognize 
                         them as a rename. The default value of 1 denotes 
                         100% similarity. The value affects what is passed as
                          --find-renames argument to git log.""")
    .option(
        'ignoreAllSpace', bool, isOptional=True, default=True,
             title='Ignore Whitespace',
             description=('Use --ignore-all-space git option. If specified '
                          'causes whitespace to be ignored when computing '
                          'line change metrics.'))
     )
    return config
