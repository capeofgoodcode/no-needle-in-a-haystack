import git as gitpy
# other imports omitted...

class DiffCounts(NamedTuple):
    ''' Number of differences as added, modified and deleted'''
    nAdd: int = 0
    nMod: int = 0
    nDel: int = 0

class Git:
    '''Pythonic interface to Git.'''

    def __init__(self,
                 gitDir: str,
                 ignoreAllSpace: bool=True):
        '''
        :param gitDir: Path to the repository.
        :param ignoreAllSpace: If :obj:`True`, uses the ``--ignore-all-space``
            git option with methods responsible for computing line based diffs
            (e.g. :meth:`diffLines`). Note that this parameter has no effect
            on the output of :meth:`log` and :meth:`_diff`. I.e. if there
            is a file with whitespace only changes, it will be still reported
            as modified even though its line based diff has no changes. This is
            git specific behavior: i.e. ``--ignore-all-space`` has no effect
            if ``--name-status`` is specified.
        '''
        self.gitDir = gitDir
        self._ignoreAllSpace = ignoreAllSpace
        # some more members...
        
    # other methods...

    def diffLines(self,
                  treeIsh1: str,
                  treeIsh2: str,
                  ignoreAllSpace: bool=None,
                  ) -> Dict[types.PosixPath, DiffCounts]:gi
        '''
        Return a dictionary containing the difference counts for each updated
        file between two commits

        :param treeIsh1: commit 1
        :param treeIsh2: commit 2
        :param ignoreAllSpace: See :meth:`__init__` for more information.
            If not specified, the value is taken from what has been specified
            in the class initialization.
        '''

        if ignoreAllSpace is None:
            ignoreAllSpace = self._ignoreAllSpace
        while True:
            output = self.gitPyCmd.diff(treeIsh1,
                                        treeIsh2,
                                        unified=0,
                                        ignore_all_space=ignoreAllSpace,
                                        detangle_retry_count=3)
            try:
                enhancer = DiffDataEnhancer(output)
                return enhancer.diffLines()
            except unidiff.UnidiffParseError as e:
               # more code omitted...
